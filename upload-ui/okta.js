const ENV = {
    dev: { CLIENT_ID: '0oa4hvk1ta1lE4MKr4x6', ISSUER: 'https://dev-491208.okta.com/oauth2/default', FLOW: 'pkce' },
    prod: { CLIENT_ID: '0oa4b175xt4Ky7ttA357', ISSUER: 'https://gitlab.okta.com', FLOW: 'pkce' }
};
const CURRENT_ENV = 'prod';
const REDIRECT_URL = window.location.origin;
const DOWNLOAD_API_URL = 'https://t5gxj7v1ak.execute-api.us-east-1.amazonaws.com';
const responseTypes = { pkce: 'code', implicit: ['id_token', 'token'] };
const grants = { pkce: 'authorization_code', implicit: 'implicit' };
const currentEnv = ENV[CURRENT_ENV];

const oktaAuth = new OktaAuth({
    issuer: currentEnv.ISSUER,
    clientId: currentEnv.CLIENT_ID,
    redirectUri: REDIRECT_URL,

    onSessionExpired: function() {
        console.log('re-authorization is required');
        oktaAuth.tokenManager.clear();
        loginOkta();
    }
});

function loginOkta() {
    oktaAuth.options.grantType = grants[currentEnv.flow];
    oktaAuth.token.getWithRedirect({
        responseType: responseTypes[currentEnv.flow],
        scopes: ['openid', 'profile', 'email']
    });
}

function getIdToken() {
    return oktaAuth.tokenManager.get('id_token');
}

function getAccessToken() {
    return oktaAuth.tokenManager.get('access_token');
}

async function getToken() {
    try {
        const token = await getIdToken();

        if (token) return token;
    } catch(e) {
        console.error(e);
    }

    oktaAuth.tokenManager.clear();
    loginOkta();
};

async function parseTokenFromUrl() {
    if (window.location.href.indexOf('error=') !== -1) {
        alert("Error signing in through Okta, please go back and try again or create an issue.");
        throw "error_signin";
    }
    if (window.location.href.indexOf('code=') === -1) return;

    const urlResponse = await oktaAuth.token.parseFromUrl();
    const tokens = urlResponse.tokens;

    if (tokens.idToken) {
        await oktaAuth.tokenManager.add('id_token', tokens.idToken);
    }

    if (tokens.accessToken) {
        await oktaAuth.tokenManager.add('access_token', tokens.accessToken);
    }
}

async function main() {
    await parseTokenFromUrl();

    const token = await getToken();

    if (!token) return;

    const email = token.claims.email;
    displayUserEmail(email);

    document.forms[0].onsubmit = (event) => {
        event.preventDefault();

        const ticketId = document.getElementById('ticketId').value;
        callTicketUploadApi(ticketId);
    };
}

const displayUserEmail = (email) => {
    document.querySelectorAll('.user-email').forEach((element) => {
        element.innerHTML = email;
    });
};

const callTicketUploadApi = async (ticketId) => {
    const token = await getIdToken();

    try {
        const response = await fetch(
            `${DOWNLOAD_API_URL}/generateTicketUpload?ticket=${ticketId}`,
            {
                headers: {
                    Authorization: `${token.value}`
                },
            }
        );

        const blob = await response.blob();
        const fileName = response.headers.get('content-disposition').split('filename=')[1];
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', fileName);

        document.body.appendChild(link);
        link.click();
        link.parentNode.removeChild(link);
    } catch (error) {
        console.log(error);
    }
};

window.onload = main;
