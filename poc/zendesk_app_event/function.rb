# frozen_string_literal: true

require 'aws-sdk-s3'
require 'base64'
require 'json'

TICKET_REGEX = /\A(TS)?\d+\z/.freeze

def log(message)
  p message
end

def respond(message, status_code = 200)
  log "LOG(#{status_code}): #{message}"
  { statusCode: status_code, body: JSON.generate(message) }
end

def handler(event:, context:)
  token = event.dig('queryStringParameters', 'token')
  if !token.nil? && token == ENV['ZENDESK_AUTH_TOKEN']
    authenticated_handler(event: event, context: context)
  else
    respond('Unauthorized.', 401)
  end
end

def authenticated_handler(event:, context:)
  action = event.dig('queryStringParameters', 'action')
  ticket_id = event.dig('queryStringParameters', 'ticket_id')
  filename = event.dig('queryStringParameters', 'filename')
  path = event.dig('queryStringParameters', 'path')

  log "Handling: #{action} for #{ticket_id} with filename '#{filename}' at path #{path}."
  return respond("Wrong ticket ID format: '#{ticket_id}'.", 400) if ticket_id.nil? || !ticket_id.match(TICKET_REGEX)

  case action
  when 'list_files'
    list_files(ticket_id, path)
  when 'download_file'
    download_file(ticket_id, filename, path)
  else
    respond("Undefined action #{action}.", 405)
  end
end

def download_file(ticket_id, filename, path)
  return respond("Invalid filename: #{filename}", 400) if filename.nil? || filename == ''

  prefix = ticket_dir(ticket_id, path)
  object = s3_client.object("#{prefix}#{filename}")

  return respond("Filename does not exist: #{filename}", 404) unless object.exists?

  expires_in_minutes = 1
  presigned_url = object.presigned_url(:get, expires_in: expires_in_minutes * 60)
  respond(
    {
      key: filename,
      url: presigned_url
    }
  )
end

def list_files(ticket_id, path)
  prefix = ticket_dir(ticket_id, path)
  objects = s3_client.objects(
    {
      delimiter: '/',
      prefix: prefix
    }
  ).map do |object|
    { key: object.key.sub(/^#{prefix}/, ''), size: object.size }
  end.filter do |object|
    !object[:key].empty?
  end

  respond(objects)
end

def s3_client
  region = ENV['AWS_REGION']
  bucket = ENV['BUCKET_NAME']

  Aws::S3::Resource.new(region: region).bucket(bucket)
end

def ticket_dir(ticket_id, prefix)
  prefix = 'upload' if prefix.empty?
  "#{prefix}/#{ticket_id}/"
end
