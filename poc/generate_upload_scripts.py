#!/usr/bin/python

import os
import sys
import getopt
import re
import logging
import boto3
from botocore.exceptions import ClientError


def usage():
    print os.path.basename(__file__) + ' -t <ticket_number>'
    sys.exit()


def process_args(argv):
    ticket_number = ''
    try:
        opts, args = getopt.getopt(argv, "ht:", ["ticket="])
    except getopt.GetoptError:
        usage()
    for opt, arg in opts:
        if opt == '-h':
            usage()
        elif opt in ("-t", "--ticket"):
            ticket_number = arg

    if not ticket_number:
        usage()

    return(ticket_number)


def create_presigned_post(bucket_name, object_name,
                          fields=None, conditions=None, expiration=3600):
    """Generate a presigned URL S3 POST request to upload a file

    :param bucket_name: string
    :param object_name: string
    :param fields: Dictionary of prefilled form fields
    :param conditions: List of conditions to include in the policy
    :param expiration: Time in seconds for the presigned URL to remain valid
    :return: Dictionary with the following keys:
        url: URL to post to
        fields: Dictionary of form fields and values to submit with the POST
    :return: None if error.
    """

    # Generate a presigned S3 POST URL
    s3_client = boto3.client('s3')
    try:
        response = s3_client.generate_presigned_post(bucket_name,
                                                     object_name,
                                                     Fields=fields,
                                                     Conditions=conditions,
                                                     ExpiresIn=expiration)
    except ClientError as e:
        logging.error(e)
        return None

    # The response contains the presigned URL and required fields
    return response


ticket_number = process_args(sys.argv[1:])
print 'Ticket number: ' + ticket_number

res = create_presigned_post('roconnor-test-bucket1', 'upload/' + ticket_number + '/${filename}')

html_templ = """<html>
  <head>
    <title>Gitlab Support Upload Form</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  </head>
  <body>
	  <table>
		  <tr>
			  <td rowspan="2">
	  <svg id="logo_art" data-name="logo art" xmlns="http://www.w3.org/2000/svg" width="100" viewBox="0 0 865 784"><defs><style>.cls-1{fill:#8c929d;}.cls-2{fill:#fc6d26;}.cls-3{fill:#e24329;}.cls-4{fill:#fca326;}</style></defs><title>gitlab-logo-gray-stacked-rgb</title><path id="path14" class="cls-1" d="M439.25,496.83h-21.8l.1,162.5h88.3v-20.1h-66.5l-.1-142.4Z"/><g id="g24"><path id="path26" class="cls-1" d="M589.87,630.82a37.2,37.2,0,0,1-27,11.4c-16.6,0-23.3-8.2-23.3-18.9,0-16.1,11.2-23.8,35-23.8a104.65,104.65,0,0,1,15.4,1.2v30.1Zm-22.6-98.5a72.19,72.19,0,0,0-46.4,16.7l7.7,13.4c8.9-5.2,19.8-10.4,35.5-10.4,17.9,0,25.8,9.2,25.8,24.6v7.9a99.42,99.42,0,0,0-15.1-1.2c-38.2,0-57.6,13.4-57.6,41.4,0,25.1,15.4,37.7,38.7,37.7,15.7,0,30.8-7.2,36-18.9l4,15.9h15.4v-83.2c-.1-26.3-11.5-43.9-44-43.9Z"/></g><g id="g28"><path id="path30" class="cls-1" d="M681.61,643c-8.2,0-15.4-1-20.8-3.5v-75.1c7.4-6.2,16.6-10.7,28.3-10.7,21.1,0,29.2,14.9,29.2,39,0,34.2-13.1,50.3-36.7,50.3m9.2-110.6a39.93,39.93,0,0,0-30,13.3v-21l-.1-27.8h-21.3l.1,158.5c10.7,4.5,25.3,6.9,41.2,6.9,40.7,0,60.3-26,60.3-70.9-.1-35.5-18.2-59-50.2-59"/></g><g id="g32"><path id="path34" class="cls-1" d="M201.89,514.46c19.3,0,31.8,6.4,39.9,12.9l9.4-16.3c-12.7-11.2-29.9-17.2-48.3-17.2-46.4,0-78.9,28.3-78.9,85.4,0,59.8,35.1,83.1,75.2,83.1a127.79,127.79,0,0,0,48.4-9.4l-.5-63.9V569h-59.5v20.1h38l.5,48.5c-5,2.5-13.6,4.5-25.3,4.5-32.2,0-53.8-20.3-53.8-63-.1-43.5,22.2-64.6,54.9-64.6"/></g><g id="g36"><path id="path38" class="cls-1" d="M355.41,496.81h-21.3l.1,27.3v94.3c0,26.3,11.4,43.9,43.9,43.9a70.14,70.14,0,0,0,13.1-1.2V642a62.36,62.36,0,0,1-9.9.7c-17.9,0-25.8-9.2-25.8-24.6v-65h35.7v-17.8h-35.7l-.1-38.5Z"/></g><path id="path40" class="cls-1" d="M280,659.33h21.3v-124H280v124Z"/><path id="path42" class="cls-1" d="M280,518.23h21.3v-21.3H280v21.3Z"/><g id="g44"><path id="path46" class="cls-2" d="M600.67,301.81l-18.91-58.12L544.34,128.41a6.47,6.47,0,0,0-12.27,0L494.65,243.62H370.32L332.9,128.41a6.46,6.46,0,0,0-12.26,0L283.28,243.62l-18.91,58.19A12.87,12.87,0,0,0,269,316.2L432.49,435,595.94,316.2a12.89,12.89,0,0,0,4.73-14.39"/></g><g id="g48"><path id="path50" class="cls-3" d="M432.53,434.89h0l62.16-191.28H370.37l62.16,191.28Z"/></g><g id="g56"><path id="path58" class="cls-2" d="M432.49,434.9,370.32,243.61h-87L432.49,434.9Z"/></g><g id="g64"><path id="path66" class="cls-4" d="M283.25,243.68h0L264.34,301.8A12.89,12.89,0,0,0,269,316.19L432.46,435,283.25,243.68Z"/></g><g id="g72"><path id="path74" class="cls-3" d="M283.28,243.68h87.11L332.9,128.47a6.47,6.47,0,0,0-12.27,0L283.28,243.68Z"/></g><g id="g76"><path id="path78" class="cls-2" d="M432.53,434.9l62.16-191.29H581.8L432.53,434.9Z"/></g><g id="g80"><path id="path82" class="cls-4" d="M581.74,243.68h0l18.91,58.12A12.86,12.86,0,0,1,596,316.19L432.54,434.89l149.2-191.21Z"/></g><g id="g84"><path id="path86" class="cls-3" d="M581.78,243.68h-87.1L532.1,128.47a6.46,6.46,0,0,1,12.26,0l37.42,115.21Z"/></g></svg>
			  </td>
			  <td>
    <!-- Copy the 'url' value returned by S3Client.generate_presigned_post() -->
    <form action="%%URL%%" method="post" enctype="multipart/form-data">
      <!-- Copy the 'fields' key:values returned by S3Client.generate_presigned_post() -->
      <input type="hidden" name="key" value="%%KEY%%" />
      <input type="hidden" name="AWSAccessKeyId" value="%%AWSACCESSKEYID%%" />
      <input type="hidden" name="policy" value="%%POLICY%%" />
      <input type="hidden" name="signature" value="%%SIGNATURE%%" />
    File:
      <input type="file"   name="file" /> <br />
      			  </td>
	          </tr>
		  <tr>
			  <td>
      <input type="submit" style="color:#fff;background-color:#1aaa55;border-color:#1aaa55" name="submit" value="Upload to Gitlab Support" />

    </form>
    			  </td>
		  </tr>
	  </table>
  </body>
</html>"""

bash_templ = """#!/bin/bash

function usage
{
    echo "usage: $0 <filename>"
    exit 1
}

if [[ -n $1 ]]; then
    file=$1
else
    usage
fi

if ! [[ -f $file ]]; then
    echo "Can't find file: $file !"
    usage
fi

res=$(type curl 2>&1)
if [[ $? -ne 0 ]]; then
    echo "Error: Unable to find curl command (required for uploading)."
    exit 1
fi

curl_cmd=$(cat <<'EOCURL'
curl -s -H Content-Type=multipart/form-data -F key=%%KEY%% -F policy=%%POLICY%% -F AWSAccessKeyId=%%AWSACCESSKEYID%% -F signature=%%SIGNATURE%% -F file=@%%FILE%% %%URL%%
EOCURL
)
curl_cmd=${curl_cmd//%%FILE%%/${file}}
res=$($curl_cmd)

if [[ $? -eq 0 && $res == "" ]]; then
    echo "File $file uploaded successfully."
    exit 0
else
    echo "Error: $file failed to upload!"
    echo "$res"
    exit 1
fi
"""

html_templ = re.sub('%%URL%%', res['url'], html_templ)
html_templ = re.sub('%%KEY%%', res['fields']['key'], html_templ)
html_templ = re.sub('%%AWSACCESSKEYID%%', res['fields']['AWSAccessKeyId'], html_templ)
html_templ = re.sub('%%POLICY%%', res['fields']['policy'], html_templ)
html_templ = re.sub('%%SIGNATURE%%', res['fields']['signature'], html_templ)

bash_templ = re.sub('%%URL%%', res['url'], bash_templ)
bash_templ = re.sub('%%KEY%%', res['fields']['key'], bash_templ)
bash_templ = re.sub('%%AWSACCESSKEYID%%', res['fields']['AWSAccessKeyId'], bash_templ)
bash_templ = re.sub('%%POLICY%%', res['fields']['policy'], bash_templ)
bash_templ = re.sub('%%SIGNATURE%%', res['fields']['signature'], bash_templ)

f = open("gitlab_support.html", "w")
f.write(html_templ)
f.close()

f = open("sup_upload.sh", "w")
f.write(bash_templ)
f.close()
