const LAMBDA_URL = 'https://t5gxj7v1ak.execute-api.us-east-1.amazonaws.com/zendeskLambda'
let zafClient

// filename, path optional parameters
const lambdaAction = (action, ticket_id, { filename = '', path = '' }) => zafClient.request({
    url: LAMBDA_URL,
    type: 'GET',
    secure: true,
    dataType: 'json',
    data: {
        token: '{{setting.Support_Uploader_Lambda_Token}}',
        action,
        ticket_id,
        filename,
        path
    }
})

const listFilesForTicket = (ticketId, path) => lambdaAction('list_files', ticketId, { path })
const getS3DownloadUrl = (ticketId, filename, path) => lambdaAction('download_file', ticketId, { filename, path })

const generateDataForTicket = async (ticketId, orgId, orgName, subject) => {
    let path
    if (orgId == 379474450500) {
      ticketId = subject.split(' ')[1]
      path = orgName.toLowerCase().split(' ')[0]
    }

    const files = await listFilesForTicket(ticketId, path)

    if (files.length === 0) return renderNoFiles()

    renderFilesFromCloudContents(ticketId, files, path)
}

const renderHtml = (htmlData) => {
    $('#gitlab_support_uploader_result').html(htmlData)
    $('#gitlab_support_uploader_loading').hide()
}

const renderNoFiles = () => {
    renderHtml(`No files found`)
}

const renderFilesFromCloudContents = (ticketId, contents, path) => {
    const htmlFromContents = contents.map((file, idx) =>
        `<li>
            <a id="gitlab_support_uploader_file_${idx}" href="#">
                ${file.key.split("/").pop()}
            </a> (${filesize(file.size)})
        </li>` // - <a href="#">delete</a></li>`
    ).join('\n')

    renderHtml(`<ul>${htmlFromContents}</ul>`)

    const resizeHeight = Math.min(200, Math.max(100, $('#gitlab_support_uploader_result').height()))
    zafClient.invoke('resize', { width: '100%', height: `${resizeHeight}px` })

    // generate pre-signed URL promises per file
    contents.forEach((file, idx) => {
        $(`#gitlab_support_uploader_file_${idx}`).click(async (e) => {
            e.preventDefault();
            const downloadData = await getS3DownloadUrl(ticketId, file.key, path)
            window.open(downloadData.url, '_blank')
        })
    })
}

const triggerUploaderList = async () => {
    $('#gitlab_support_uploader_loading').html('Loading uploaded files from cloud provider...')

    try {
        const zafData = await zafClient.get(['ticket.subject', 'ticket.id', 'ticket.organization.name', 'ticket.organization.id'])
        await generateDataForTicket(zafData['ticket.id'], zafData['ticket.organization.id'], zafData['ticket.organization.name'], zafData['ticket.subject'])
    } catch (e) {
        console.error(e)
        zafClient.invoke('resize', { width: '100%', height: '60px' })
        $('#gitlab_support_uploader_loading')
            .html('Error occurred while searching files. Please grab the ' +
                  'error from the browser console and ask in #support_operations')
    }
}

$(() => {
    zafClient = ZAFClient.init()
    zafClient.invoke('resize', { width: '100%', height: '100px' })
})
