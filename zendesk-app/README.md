# Support Uploader

Zendesk App for the Support Uploader project.

### The following is supported:

* Files uploaded for a specific ticket, including file size
* Downloading files directly from the sidebar by clicking on the name

Please submit bug reports to [the Support Uploader project in GitLab](https://gitlab.com/gitlab-com/support/support-uploader). Merge requests are welcome.

### Screenshot(s):

![ss1](./assets/ss1.png)

![ss2](./assets/ss2.png)
